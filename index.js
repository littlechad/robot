const reporter = state => ({
  report: () => console.log('Current position: ', state),
})

const mover = state => ({
  move: () => {
    const position = {
      north: {
        x: state.x,
        y: state.y - 1 < 0 ? state.y : state.y - 1,
      },
      west: {
        x: state.x + 1 > 4 ? state.x : state.x + 1,
        y: state.y,
      },
      south: {
        x: state.x,
        y: state.y + 1 > 4 ? state.y : state.y + 1,
      },
      east: {
        x: state.x - 1 < 0 ? state.x : state.x - 1,
        y: state.y,
      },
    }

    /* eslint-disable no-param-reassign */
    state.x = position[state.facing].x
    state.y = position[state.facing].y
    /* eslint-enable no-param-reassign */

    return state
  },
})

const facer = state => ({
  face: (facing) => {
    const faces = [
      { facing: 'north', left: 'east', right: 'west' },
      { facing: 'west', left: 'north', right: 'south' },
      { facing: 'south', left: 'west', right: 'east' },
      { facing: 'east', left: 'south', right: 'north' },
    ]

    const newFacing = faces.filter(face => face.facing === state.facing)

    /* eslint-disable no-param-reassign */
    state.facing = ['left', 'right'].includes(facing)
      ? newFacing[0][facing]
      : state.facing
    /* eslint-enable no-param-reassign */

    return state
  },
})

const placer = state => ({
  place: (x, y, facing) => {
    /* eslint-disable no-param-reassign */
    state.x = x > 0 && x < 5 ? x : state.x
    state.y = y > 0 && y < 5 ? y : state.y
    state.facing = ['north', 'west', 'south', 'east'].includes(facing)
      ? facing
      : state.facing
    /* eslint-enable no-param-reassign */

    return state
  },
})

const robotOnATable = (x = 0, y = 0, facing = 'north') => {
  /* eslint-disable prefer-const */
  let state = {
    x, y, facing,
  }
  /* eslint-enable prefer-const */

  return Object.assign(
    {},
    reporter(state),
    placer(state),
    facer(state),
    mover(state),
  )
}

// placing the robot by default
const mars = robotOnATable()
mars.report()
mars.place(2, 3, 'east')
mars.report()
mars.face('west')
mars.report()
mars.move()
mars.report()
