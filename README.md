# Toy Robot Simulator

A javascript Composition over Inheritance approach to solve the Toy Robot Simulator [problem](https://bitbucket.org/littlechad/robot/src/master/PROBLEM.md)

> I will wander the wilderness. ~ TRS

## Getting started

`$ yarn`

## Running

`$ yarn start`
